package main

import (
	"fmt"
)

func fibobacci() func() int {
	a, b := 0, 1
	return func() int {
		a, b = b, a+b
		return b
	}
}

func main() {
	f := fibobacci()
	for i := 0; i < 10; i++ {
		fmt.Println(f())
	}
}
