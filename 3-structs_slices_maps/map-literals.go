//Map literals are like struct liteals but keys are required

package main

import "fmt"

type Vertex struct {
	Lat, Long float64
}

var m = map[string]Vertex{
	"Bell Labs": Vertex{
		40.68433, -74.39967,
	},
	"Google Labs": Vertex{
		37.42202, -122.08408,
	},
}

func main() {
	fmt.Println(m)
}
