//Stringer is a type that can describe itself as a string

package main

import "fmt"

type Person struct {
	Name string
	Age  int
}

func (p Person) String() string {
	return fmt.Sprintf("%v (%v years)", p.Name, p.Age)
}

func main() {
	a := Person{"Kathurima Kimathi", 42}
	b := Person{"Kend P. K", 27}
	fmt.Println(a, b)
}
